# kylin-lock

#### 介绍
麒麟分布式锁-支持redisson、zookeeper

#### 软件架构
软件架构说明
1.  参考 [lock4j](https://gitee.com/baomidou/lock4j.git)
2.  spring boot 2.6.3
3.  支持redisson、zookeeper分布式锁
4.  redisson支持重入锁、读写锁、联锁、红锁、公平锁
5.  zookeeper支持重入锁、不可重入锁、读写锁、联锁
6.  支持注解、方法调用、函数式编程加锁
7.  支持多注解，对同一方法加不同的锁
8.  keys、keySuffix，支持SpEL表达式
9.  支持自定义全局、方法级加锁失败回调
10. 方法级失败回调：方法名、参数类型列表相同，支持相同类型的返回
#### 使用说明
1. 引入依赖
    ~~~
    <dependency>
        <groupId>com.gitee.wangjkui</groupId>
        <artifactId>kylin-lock-spring-boot-starter</artifactId>
        <version>Latest Version</version>
    </dependency>
    ~~~

2. 配置
    ~~~
    spring:
      redis:
        redisson:
          file: classpath:redisson.yml
    #    host: 127.0.0.1
    #    port: 6379
    #    database: 1
    #    timeout: 30000
    #    password: redis123
    
    #分布式锁
    kylin:
      lock:
        acquire-timeout: 3000 #默认值，可不设置 单位：毫秒
        expire: 30000 #默认值，可不设置 单位：毫秒
        retry-interval: 100 # 获取锁失败时重试时间间隔 单位：毫秒
        lock-key-prefix: kylin-lock # 默认配置
        primary-executor: com.wjk.kylin.lock.executor.redisson.RedissonLockExecutor #默认redisson>zookeeper
        redisson: true # 开启redisson
        zookeeper:
          zk-servers: 127.0.0.1:2181
          session-timeout: 60000
          connection-timeout: 60000
          base-sleep-time-ms: 5000
          max-retries: 3
          namespace: curator/kylin/lock

    ~~~

3. 重入锁
   ~~~
   @KylinLock(name = "reentrant_key", expire = 60000)
   public void demoMethod2() {
       System.out.println("demoMethod2 - start" + getClass());
       try {
           Thread.sleep(4000);
       } catch (InterruptedException e) {
           e.printStackTrace();
       }
       //重入锁
       indexService.demoMethod2();
       System.out.println("demoMethod2 - end " + getClass());
   }
   ~~~
4. 读写锁
   ~~~
    @Override
    @KylinLock(name = "read-write", acquireTimeout = 0, lockType = LockType.READ)
    public void read1(String key) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("执行方法read1 , 当前线程:" + Thread.currentThread().getName() + "threadId:" + Thread.currentThread().getId());
    }

    @Override
    @KylinLock(name = "read-write", acquireTimeout = 0, lockType = LockType.WRITE)
    public void write1(String key) {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("执行方法write1 , 当前线程:" + Thread.currentThread().getName());
    }
   ~~~
   
5. 多注解
   ~~~
   @KylinLock(name = "reentrant_key1", expire = 60000)
   @KylinLock(name = "reentrant_key2", expire = 60000)
   @KylinLock(name = "reentrant_key3", expire = 60000)
   public void demoMethod3() {
       System.out.println("demoMethod3 - start" + getClass());
       try {
           Thread.sleep(4000);
       } catch (InterruptedException e) {
           e.printStackTrace();
       }
       System.out.println("demoMethod3 - end " + getClass());
   }
   ~~~
6. 联锁
   ~~~
   @KylinLock(lockType = LockType.MULTI)
   public void demoMethod6() {
       System.out.println("demoMethod6 - start" + getClass());
       try {
           Thread.sleep(40000);
       } catch (InterruptedException e) {
           e.printStackTrace();
       }
       System.out.println("demoMethod6 - end " + getClass());
   }
   ~~~
7. 红锁 
   ~~~
   @KylinLock(lockType = LockType.RED, keySuffix = {"#user.id", "'red2'"}, executor = RedissonLockExecutor.class)
   public void demoMethod11(User user) {
       System.out.println("demoMethod11 - start" + getClass());
       try {
           Thread.sleep(40000);
       } catch (InterruptedException e) {
           e.printStackTrace();
       }

       System.out.println("demoMethod11 - id:" + user.getId() + ",name:" + user.getName());
   }
   ~~~
8. 公平锁
   ~~~
   @KylinLock(name = "fair_lock_key", lockType = LockType.FAIR, executor = RedissonLockExecutor.class)
   public void demoMethod8() {
       System.out.println("demoMethod8 - start" + getClass());
       try {
           Thread.sleep(4000);
       } catch (InterruptedException e) {
           e.printStackTrace();
       }
       indexService.demoMethod8();
       System.out.println("demoMethod8 - end " + getClass());
   }
   ~~~
9. 函数式编程
   ~~~
   public void execute1(String key) {
       Integer num = lockTemplate.execute(key, IndexServiceImpl::getNumber);
       System.out.println("执行execute方法1 , 当前线程:" + Thread.currentThread().getName() + " , 获取的返回值是：" + num);
   }
   public static Integer getNumber() {
       try {
           Thread.sleep(40000);
       } catch (InterruptedException e) {
           e.printStackTrace();
       }
       System.out.println("执行getNumber方法 , 当前线程:" + Thread.currentThread().getName());
       return 1;
   }
   ~~~
10. 自定义加锁失败执行方法
    ~~~
    //加锁失败，会调用 DemoLockFailureCallBack中方法名称、参数类型列表相同的方法
    @KylinLock(acquireTimeout = 0, lockFailure = DemoLockFailureCallBack.class)
    public Integer demoMethod14(Integer num) {
        System.out.println("demoMethod12 - start,num:" + num);
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("demoMethod12 - end,num:" + num);
        return num * 2;
    }
    @Slf4j
    @Component
    public class DemoLockFailureCallBack implements LockFailureCallBack {
        //方法名称、参数类型列表相同
        public Integer demoMethod14(Integer num) {
            log.error("demoMethod14-方法自定义失败回调,入参num:{}", num);
            return -1;
        }
    }
    ~~~

#### 特别鸣谢

1.  开源项目[lock4j](https://gitee.com/baomidou/lock4j.git)
