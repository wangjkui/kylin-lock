package com.wjk.kylin.lock.example.custom;

import com.wjk.kylin.lock.key.DefaultLockKeyBuilder;
import org.springframework.stereotype.Component;

/**
 * 自定义lock key builder
 *
 * @author wangjinkui
 */
@Component
public class CustomLockKeyBuilder extends DefaultLockKeyBuilder {


}
