package com.wjk.kylin.lock.example;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LockSamplesApplication {

    public static void main(String[] args) {
        SpringApplication.run(LockSamplesApplication.class, args);
    }

}
