package com.wjk.kylin.lock.example.custom;

/**
 * @author wangjinkui
 */
public class BusinessException extends RuntimeException {

    public BusinessException() {
        super();
    }

    public BusinessException(String message) {
        super(message);
    }
}
