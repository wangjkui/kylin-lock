package com.wjk.kylin.lock.example.custom;

import com.wjk.kylin.lock.fail.LockFailureCallBack;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;


/**
 * 自定义获取锁异常处理
 *
 * @author wangjinkui
 */
@Slf4j
@Component
public class DemoLockFailureCallBack implements LockFailureCallBack {
    public void demoMethod12(String name) {
        log.error("demoMethod12-方法自定义失败回调,name:{}", name);
    }

    public void demoMethod13() {
        log.error("demoMethod13-方法自定义失败回调");
        throw new BusinessException("请求太快啦~");
    }

    public Integer demoMethod14(Integer num) {
        log.error("demoMethod14-方法自定义失败回调,入参num:{}", num);
        return -1;
    }
}
