package com.wjk.kylin.lock.example.controller;


import com.wjk.kylin.lock.example.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class TestController {

    @Autowired
    private IndexService indexService;

    @GetMapping("/test")
    public String test() {
        return new Date().toString();
    }
}
